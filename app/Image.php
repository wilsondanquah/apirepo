<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table ='images';

    protected $fillable =['name','article_id']; 
   

    //One Article has many images
    public function article(){
        return $this->belongsTo(Article::class);
    }
}
