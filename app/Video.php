<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table ='videos';

    protected $fillable =['name','article_id']; 
   

    //A video belongs to only one article
    public function article(){
        return $this->belongsTo(Article::class);
    }
}
