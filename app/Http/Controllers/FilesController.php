<?php
namespace App\Http\Controllers;
use App\Images;
use Illuminate\Http\Request;
class FilesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['only'=>['destroy','create']]);

    }
    /**
     * Create a new controller instance.
     *
     * @return void
     * 
     */
    

    public function index()
    {
     
     $articles = Article::all();
     return response()->json($articles);
    }

     public function create(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        $articles = new Article;
       $articles->title= $request->title;
       $articles->body = $request->body;
       $articles->save();
       return response()->json(["message"=>"Article saved succesfully"],200);
     }

     public function show($id) {
        $articles = Article::find($id);
        return response()->json($articles);
     }


     public function update(Request $request, $id){ 
    try{  
         $article = Article::findorFail($id);
         $article->title = $request->title;
         $article->body = $request->body;
         $article->save();
        // $article = Article::findorFail($id);
        // Article::where('id','=',$id)->update(array(['title'=>$request->title,'body'=>$request->body]));
       // $article->update($request->all());
        return response()->json([$article,"message"=>"Article updated"]);
    }
    catch(Exception $e){
        return response()->json("Something went wrong");
    }
     }
     
     public function destroy($id){
        $articles = Article::find($id);
        $articles->delete();
         return response()->json('Article deleted');
     }
    }
