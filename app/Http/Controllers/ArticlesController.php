<?php
namespace App\Http\Controllers;
use App\Article;
use App\Image;
use App\Video;
use Illuminate\Http\Request;
class ArticlesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except'=>['destroy','create']]);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     * 
     */
    
     //Return all  published articles
    public function index()
    {
     $articles = Article::all();
     return response()->json($articles);
    }


    public function uploadImages($images)
    {
        foreach($images as $image){
            $imageName = $image->getClientOriginalName();
            $image_ext = $image->getClientOriginalExtension();
            $timeofUpload = date("Y-m-d",time());
            $new_file_name =  $timeofUpload.rand(123456,999999).".".$image_ext;
            $destinaton_path_image = public_path('/media/images');
            $image->move($destinaton_path_image,$new_file_name);
            $image_instance = new Image;
            $image_instance->name =$new_file_name;
            $image_instance->article_id = Article::orderby('created_at','DESC')->pluck('id')->first();
            $image_instance->save();
            }  
   }
  

     
    public function create(Request $request)
   {
    //Validate Articles
    $this->validate($request, [
    'title' => 'required',
    'body' => 'required'
     ]);
     echo "We start here";

     //Save Articles
    $articles = new Article;
    $article_id = rand(123456,999999);
    $articles->id =  $article_id;
    $articles->title= $request->title;
    $articles->body = $request->body;
    $articles->save();
       

    //Check if file is uploaded and an image if so upload to media/images 
    //directory basic image formats validation included and file size of
    //maximum 10mb NB ::Validation for multiple files is not working atm h
    //hence it is commented
    if ($request->file('images'))
    { 
    $this->validate($request, [
    // 'images' => 'required|mimes:jpg,png,jpeg|max:50000000',
     ]);  
    $images = $request->file('images');
      //We get File Name and Extension and rename it to store in files folder and database
     $this->uploadImages($images);
         
     //Check if file is uploaded and a video if so upload to media/videos 
     //directory basic video formats validation included and file size of
     //maximum 10mb
    
     if ($request->file('videos'))
     { 
     $this->validate($request, [
    //  'videos' => 'required|mimes:mp4,flv,3gp|max:50000000',

      ]);
      $videos = $request->file('videos');

      $this->uploadVideos($videos);
    }
             
       return response()->json(["message"=>"Article saved succesfully"],200);   
   }}

    
   public function uploadVideos($videos)
  {
          //We get File Name and Extension and rename it to store in files folder and database
       foreach($videos as $video){     
            echo "   We passed the video validation";
            $videoName = $video->getClientOriginalName();
            $video_ext = $video->getClientOriginalExtension();
            $timeofUpload = date("Y-m-d",time());
            $new_video_name = $timeofUpload. rand(123456,999999).".".$video_ext;
            $destinaton_path_video = public_path('/media/videos');
            $video->move($destinaton_path_video,$new_video_name);
            $file_instance = new Video;
            $file_instance->name =$new_video_name;
            $file_instance->article_id = Article::orderby('created_at','DESC')->pluck('id')->first();
            $file_instance->save();
            echo "   We also done done saving the file" ;
             }      

  }

     public function show($id){
        $articles = Article::find($id);
        return response()->json($articles);
     }



     public function update(Request $request, $id){ 
    try
      {  
         $article = Article::findorFail($id);
         $article->title = $request->title;
         $article->body = $request->body;
         $article->user_id = Auth::user()->id;
         $article->save();
        return response()->json([$article,"message"=>"Article updated"]);
    }
    catch(Exception $e){
        return response()->json("Something went wrong");
    }
     }


     //delete articles

     public function destroy($id)
     {
        $articles = Article::find($id);
        $articles->delete();
         return response()->json('Article deleted');
     }


     
    public function publish(Request $request, $id)
    { 
    try
      {  
         $article = Article::findorFail($id);
         $article->published = true; 
         $article->save();
        return response()->json([$article,"message"=>"Article published"]);
      }
      catch(Exception $e)
      {
        return response()->json("Something went wrong");
      }
    }




    public function unpublish($id)
    { 
    try
      {  
         $article = Article::findorFail($id);
         $article->published = false;
         $article->save();
        return response()->json([$article,"message"=>"Article removed"]);
      }
      catch(Exception $e)
      {
        return response()->json("Something went wrong");
      }
    }
}
