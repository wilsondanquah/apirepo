<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Board;
use Illuminate\Http\Request;

class BoardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function show($boardId){
        // $board = Board::findorFail($boardId);
        $board = DB::table('boards')
        ->where('id','=', $boardId)
        ->get();
        return $board;
      }
    
    public function index(){

        return Board::all();
    }
 


     public function store(Request $request){
         Board::create([
            'name'=>$request->name,
            'user_id'=> 1,
         ]);
         return response()->json(['message'=>'success'],200);

     }
     public function update(Request $request, $boardId){
        DB::table('boards')
        ->where('id', $boardId)
        ->update(['name' => $request->name]);
      return response()->json(['message'=>'success'],200);

    }
}
