<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Article extends Model 
{
 protected $guarded =[];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','body','id', 'published',
    ];
    protected $hidden = [
        'published'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password',
    // ];

    public function images()
    {
        return $this->hasMany(Image::class);
    }
    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
