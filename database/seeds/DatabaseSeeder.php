<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
       // Fake for Articles
        $faker = Faker::create();
    	foreach (range(1,12) as $index) {
	        DB::table('articles')->insert([
            
	            'title' =>  $faker->shuffle('Blahblah').' Article',
	            'body' => $faker->paragraph,
	            'user_id' =>1,
	            'published' => true,
                'updated_at'=>\Carbon\Carbon::now(),
                'created_at'=>\Carbon\Carbon::now(),      
            ]);}
    }
}
